express = require('express')
path = require('path')
webpack = require('webpack')
app = express()
compress = require 'compression'

isDevelopment = process.env.NODE_ENV != 'production'
static_path = path.join(__dirname, 'public')

PORT = process.env.PORT or 8080

if true or not isDevelopment
  app.use(compress())
  app.use(express.static(static_path, {
    etag: true
    lastModified: true
    maxAge: 86400000 * 7
  }))
  app.get('*', (req, res) ->
    res.sendFile 'index.html', root: static_path
    return
  ).listen PORT, (err) ->
    if err
      console.log err
    console.log 'Listening at localhost:' + PORT
    return

