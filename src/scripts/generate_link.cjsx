LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ PropTypes } = require 'react'
classnames = require 'classnames'
GenerateLinkForm = require './generate_link_form.cjsx'
GenerateLinkSuccess = require './generate_link_success.cjsx'
classnames = require 'classnames'
{ retrieveUserLinks } = require './actions/api'

style =
  input:
    paddingLeft:50

  earth:
    position: 'absolute'
    fontSize: '1.4rem'
    zIndex: 2
    top:33
    color: 'black'
    left: 5

module.exports = GenerateLink = React.createClass
  displayName: 'GenerateLink'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    {}

  render: ->
    if not @props.generatedLink
      child = <GenerateLinkForm
        error={@props.linkError}
        user={@props.user}
        generateLink={@props.generateLink} />
    else
      child = <GenerateLinkSuccess
        user={@props.user}
        clearGeneratedLink={@props.clearGeneratedLink}
        generatedLink={@props.generatedLink}
        retrieveLinks={@props.retrieveLinks}
        originalUrl={@props.originalUrl} />

    pageclassnames=classnames({
      "ui", "two":$(window).width() > 768, "column", "grid", "centered",
    })

    <div className={pageclassnames}>
      {child}
    </div>

  handleKeyPress: (e) ->
    if e.key == 'Enter'
      @submit()

  submit: (url) ->
    if not url
      url = @state.originalUrl
    @props.generateLink(@state.originalUrl)

  onLinkChange: (event) ->
    url = event.target.value
    valid = URL_REGEX.test(url)
    if valid
      @submit(url)

    @setState {originalUrl: url, isValid: valid}


  testUrl: (url) ->
    URL_REGEX.test(url)

GenerateLink.propTypes = {
  generateLink: PropTypes.func.isRequired
  generatedLink: PropTypes.string
  linkError: PropTypes.string
  user: PropTypes.object
  clearGeneratedLink: PropTypes.func.isRequired
  retrieveLinks: PropTypes.func.isRequired
}

