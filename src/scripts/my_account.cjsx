{ PropTypes } = require 'react'
{ changePassword } = require './actions/api'
{ connect } = require 'react-redux'
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ coinbaseAuthRedirect,
  coinbaseAuth,
  withdrawBalance,
  coinbaseDeauthorize
  saveName
  resetPassword
  setCandidate
} = require './actions/api'
CoinbaseAuth = require './user/coinbase_auth'
Withdraw = require './user/withdraw'
ResetPasswordButton = require './user/reset_password_button'
PayoutSelector = require './politics/payout_selector'
NameEditor = require './user/name_editor'
Radium = require 'radium'

style = {
  header:
    marginBottom: 20

  coinbase:
    marginBottom: 10

  title:
    paddingLeft:'1rem'

}

MyAccount = React.createClass
  displayName: 'MyAccount'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    name: @props.currentUser?.fullName
    email: @props.currentUser?.email
    saveDisabled: true
    newCandidate: @props.currentUser.candidate

  componentWillReceiveProps: (newProps) ->
    @setState
      name: newProps.currentUser?.fullName
      email: newProps.currentUser?.email
      saveDisabled: true
      newCandidate: newProps.currentUser?.candidate

  componentDidMount: ->
    query = @props.location.query
    if query
      if query.code and query.state
        @props.coinbaseAuth(query.code, query.state)

  render: ->
    if @props.currentUser?.coinbase
      withdraw = <Withdraw user={@props.currentUser}
        withdraw={@props.withdraw}
        withdrawMessage={@props.withdrawMessage} />

    <div className='ui mobile stackable sixteen column grid'>
      <div className='computer only row'>
        <h1 style={style.title}>My account</h1>
      </div>
      <div className='five wide column'>
        <div style={style.header} className='row'>
          <h3>Profile</h3>
        </div>
        <form className='ui form'>
          <div className='field'>
            <NameEditor
              saveName={@props.saveName}
              initialName={@props.currentUser?.fullName} />
          </div>

          <div className='connectButton' className='row'>
            <ResetPasswordButton resetPassword={@props.resetPassword}
              user={@props.currentUser}
              passwordReset={@props.passwordReset} />
          </div>

        </form>

        {@props.error}
      </div>
      <div className='five wide column'>
        <PayoutSelector
          initialCandidate={@props.currentUser.candidate}
          totalCandidatePayout={@props.currentUser.totalCandidatePayout}
          setCandidate={@props.setCandidate} />
      </div>
      <div className='six wide column'>
        <div className='row'>
          <div style={style.coinbase}>
            <h3>Coinbase Account</h3>
            <CoinbaseAuth
              user={@props.currentUser}
              deauthorize={@props.coinbaseDeauth}
              coinbaseAuthRedirect={@props.coinbaseAuthRedirect} />
            {withdraw}
          </div>
        </div>
      </div>
    </div>

  candidateChanged: (candidate) ->
    if candidate != @state.candidate
      @setState {newCandidate: candidate}
    else
      @setState {newCandidate: null}

  saveName: (e) ->
    e.preventDefault()
    if @state.name != @props.currentUser.fullName
      @props.saveName(@state.name)


MyAccount = Radium(MyAccount)


MyAccount.propTypes = {
  currentUser: PropTypes.object.isRequired
  coinbaseAuthRedirect: PropTypes.func.isRequired
  coinbaseAuth: PropTypes.func.isRequired
  coinbaseDeauth: PropTypes.func.isRequired
  saveName: PropTypes.func.isRequired
  withdrawMessage: PropTypes.string
  setCandidate: PropTypes.func.isRequired
}

mapStateToProps = (state) ->
  {
    currentUser: state.auth.user
    passwordReset: state.auth.passwordReset
    withdrawMessage: state.coinbase.message
  }

mapDispatchToProps = (dispatch) ->
  {
    changePassword: (newPassword) ->
      dispatch(changePassword(newPassword))
    coinbaseAuthRedirect: ->
      dispatch(coinbaseAuthRedirect())
    coinbaseAuth: (code, state) ->
      dispatch(coinbaseAuth(code, state))
    withdraw: (amount) ->
      dispatch(withdrawBalance(amount))
    coinbaseDeauth: () ->
      dispatch(coinbaseDeauthorize())
    saveName: (name) ->
      dispatch(saveName(name))
    resetPassword: (email) ->
      dispatch(resetPassword(email))
    setCandidate: (candidate) ->
      dispatch(setCandidate(candidate))

  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(MyAccount)

