{ PropTypes } = require 'react'
moment = require 'moment'

style =

  copy:
    marginTop: 10
    border: '1px solid white'
    borderRadius: 25
    display: 'inline-block'
    padding: 4
    cursor: 'pointer'
  copyIcon:
    position: 'relative'
    left: 2
  metrics:
    border:'2px solid rgba(255, 255, 255, 0.5)'
    borderRadius:'3px'
  columnopacity:
    boxShadow: '-2px 0 0 0 rgba(255, 255, 255, 0.5)'
  rowopacity:
    boxShadow: '0 -2px 0 0 rgba(255, 255, 255, 0.5)'
  borderless:
    boxShadow: '0 0 0 0 rgba(255, 255, 255, 0.5)'
  metricslink:
    marginTop:'1.5rem !important'
    marginBottom:'0.3rem !important'
    textAlign: 'center'
  copyiconcontainer:
    float:'right'
    marginTop:'10%'
  icons:
    boxShadow:'0 0 0 .05em rgba(255,255,255,.3) inset'
    WebkitTransform: 'scale(2,2); /* Safari */'
    transform: 'translateY(10%)scale(2,2)'
  shareiconcontainer:
    float:'left'
    marginTop:'10%'
  date:
    textAlign:'center'
    opacity:'0.8'
    fontSize:'0.8rem'
    marginBottom:'1.4rem'
  metricsnumberslink:
    marginTop:'0.5rem !important'
    marginBottom:'0.3rem !important'
  metricsnumberstext:
    textAlign:'center'
    textTransform:'uppercase'
    opacity:'0.7'
    letterSpacing:'1'
    fontSize:'0.7rem'
    marginBottom:'0.5rem'



module.exports = LinkMetrics =  React.createClass
  displayName: 'LinkMetrics'
  render: ->
    <div style={style.metrics} className='ui internally celled grid'>
      <div className='row'>
        <div style={style.borderless} className='sixteen wide column'>
          <h2 style={style.metricslink} className='ui center aligned header'>{@formatUrl(@props.link?.shortUrl)}</h2>
          <div style={style.date} className=''>
            Created {@formatDate(@props.link?.date)}
          </div>
        </div>
      </div>
      <div style={style.rowopacity} className='row'>
        <div className='eight wide column'>
          <h3 style={style.metricsnumberslink} className='ui center aligned header'>{@props.link?.impressions}</h3>
          <div style={style.metricsnumberstext} className=''>
            Total number of views</div>
          </div>
        <div style={style.columnopacity} className='eight wide column'>
          <h3 style={style.metricsnumberslink} className='ui center aligned header'>{@props.link?.clicks}</h3>
          <div style={style.metricsnumberstext} className=''>
            Total number ad clicks
          </div>
        </div>
      </div>
    </div>

  formatDate: (date) ->
    moment(date).format('MMM Do')

  formatUrl: (url) ->
    if __DEVELOPMENT__ and url
      return "truvi.co/"+url.substr(url.lastIndexOf('/')+1)
    if url
      url.substr(url.indexOf('://')+3)

LinkMetrics.PropTypes = {
  link: PropTypes.object.isRequired
}

