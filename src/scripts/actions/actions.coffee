Actions = {
  loginClicked: (email) ->
    {
      type: 'PERFORM_LOGIN'
      email
    }

  registerFailed: (error) ->
    type: 'REGISTER_FAIL'
    error: error

  passwordSetFailed: (error) ->
    type: 'PASSWORD_SET_FAILED'
    error: error

  receiveLogin: (res) ->
    user = res.user
    error = res.error
    token = res.token

    if token
      type = 'LOGIN_SUCCESS'
    else
      type = 'LOGIN_FAIL'

    {
      type: type
      user
      error
      token
    }

  registerClicked: (email) ->
    {
      type: 'REGISTER_CLICKED'
      email
    }

  registerReceived: (res) ->
    user = res.user
    error = res.error
    token = res.token

    if user
      type = 'REGISTER_SUCCESS'
    else
      type = 'REGISTER_FAIL'

    {
      type: type,
      user
      error
      token
    }

  clearGeneratedLink: ->
    type: 'CLEAR_GENERATED_LINK'

  generateLinkClicked: (originalUrl) ->
    {
      type: 'GENERATE_LINK_CLICKED'
      originalUrl
    }

  receiveGeneratedLink: (res) ->
    originalUrl = res.originalUrl
    shortUrl = res.shortUrl
    error = res.error
    if error
      type = 'GENERATE_LINK_FAIL'
    else
      type = 'GENERATE_LINK_SUCCESS'

    {
      type: type
      originalUrl
      shortUrl
      error
    }

  receiveGeneratedLinkAnonyous: (res) ->
    originalUrl = res.originalUrl
    shortUrl = res.shortUrl
    error = res.error
    if error
      type = 'GENERATE_LINK_FAIL_ANONYMOUS'
    else
      type = 'GENERATE_LINK_SUCCESS_ANONYMOUS'

    {
      type: type
      id: res.id
      originalUrl
      shortUrl
      error
    }

  receiveLinks: (links) ->

    {
      type: 'RECEIVE_LINKS'
      links
    }

  passwordChanged: (json) ->
    {
      type: 'PASSWORD_CHANGED'
    }

  refreshUserReceived: (json) ->
    user = json.user
    {
      type: 'REFRESH_USER_RECEIVED'
      user
    }

  coinbaseAuthReceived: (json) ->
    success = json.success

    if success
      type = 'COINBASE_AUTH_SUCCESS'
    else
      type = 'COINBASE_AUTH_FAIL'

    {
      type: type
    }

  loggedOut: ->
    {
      type: 'LOGGED_OUT'
    }

  logout: ->
    window.sessionStorage.removeItem('accessToken')
    window.localStorage.removeItem('accessToken')
    return (dispatch) ->
      dispatch(Actions.loggedOut())

  pendingPayoutsReceived: (json) ->
    type = "PENDING_PAYOUTS_RECEIVED"
    users = json.users
    return {
      type
      users
    }

  setPayouts: (payouts) ->
    type = 'SET_PAYOUTS'
    return {
      type
      payouts
    }

  confirmPayoutsReceived: (json) ->
    type = 'CONFIRM_PAYOUTS_RECEIVED'
    return {
      type
    }

  withdrawBalanceReceived: (json) ->
    type = 'WITHDRAW_BALANCE_RECEIVED'
    {
      type
      success: json.success
      error: json.error
    }

  coinbaseDeauthorizeReceived: (json) ->
    type: 'COINBASE_DEAUTHORIZE_RECEIVED'

  linkSelected: (link) ->
    type: 'LINK_SELECTED'
    selectedLink: link

  nameUpdated: (json) ->
    type: 'NAME_UPDATED'

  userVerified: (json) ->
    if json.success
      type = 'EMAIL_VERIFY_SUCCESS'
    else
      type = 'EMAIL_VERIFY_FAIL'

    type:type
    token: json.token
    user: json.user

  passwordReset: (json) ->
    type: 'PASSWORD_RESET'

  passwordSet: (json) ->
    if json.success
      type = 'PASSWORD_SET'
    else
      type = 'PASSWORD_SET_FAILED'

    type: type
    error: json.error

  candidateSet: (json) ->
    if json.success
      type = 'CANDIDATE_SET_SUCCESS'
    else
       type = 'CANDIDATE_SET_FAIL'

    type: type
    error: json.error
    candidate: json.candidate


}

module.exports = Actions






