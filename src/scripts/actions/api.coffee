actions = require './actions'
fetch = require 'isomorphic-fetch'
{ pushState } = require 'redux-router'
config = require '../config'
querystring = require 'querystring'

BASE_URL = config.BACKEND_URL
LOGIN_URL = 'login'
REGISTER_URL = 'register'
GENERATE_LINK_URL = 'api/generateLink'
GENERATE_LINK_URL_NO_AUTH = 'generateLink'
GET_LINKS_URL = 'api/getLinks'
CHANGE_PASSWORD_URL = 'api/changePassword'
GET_USER_URL = 'api/getUser'
COINBASE_AUTH_REDIRECT_URL = 'api/coinbase/url'
COINBASE_AUTH_URL = 'api/coinbase/confirm'
COINBASE_DEAUTH_URL = 'api/coinbase/deauthorize'
GET_PENDING_PAYOUTS_URL = 'api/admin/pendingPayouts'
CONFIRM_PAYOUTS_URL = 'api/admin/confirmPayouts'
WITHDRAW_BALANCE_URL = 'api/withdraw'
UPDATE_NAME_URL = 'api/updateName'
VERIFY_USER_URL = 'user/verify/'
RESET_PASSWORD_URL = 'user/resetPassword'
SET_PASSWORD_URL = 'user/setPassword'
SET_CANDIDATE_URL = 'api/user/setCandidate'

get = (url, params) ->
  return request("#{url}?#{querystring.stringify(params)}", null, 'get')

post = (url, params) ->
  return request(url, params, 'post')

request = (url, params, method) ->
  args = {
    method: method
    headers: {
      'Accept': 'application/json'
      'Content-Type': 'application/json'
      'x-access-token': getToken()
    }
    mode: 'cors'
  }
  if method == 'post'
    args.body = JSON.stringify(params)

  return fetch(BASE_URL + url, args).then((req) ->
    req.json()
  )

TOKEN = null
saveToken = (token, rememberMe=false) ->
  try
    if rememberMe
      window.localStorage.accessToken = token
    else
      window.sessionStorage.accessToken = token
  catch
    TOKEN = token

getToken = ->
  return TOKEN or window.sessionStorage.accessToken or window.localStorage.accessToken

API = {
  retrieveUserLinks: ->
    return (dispatch) ->
      get(GET_LINKS_URL).then (json) ->
        dispatch actions.receiveLinks(json.links)

  generateLink: (url, auth=true) ->
    if auth
      apiUrl = GENERATE_LINK_URL
    else
      apiUrl = GENERATE_LINK_URL_NO_AUTH
    return (dispatch) ->
      dispatch(actions.generateLinkClicked(url))
      post(apiUrl,
        originalUrl: url
      ).then((json) ->
        dispatch(actions.receiveGeneratedLink(json))
        if not auth
          dispatch(actions.receiveGeneratedLinkAnonyous(json))
          ga('send', 'event', 'link', 'generateNoAuth')
        else
          ga('send', 'event', 'link', 'generate')
      )

  refreshUser: ->
    return (dispatch) ->
      get(GET_USER_URL).then (json) ->
        dispatch(actions.refreshUserReceived(json))

        if json.user
          ga('set', 'userId', json.user.id)
          ga('send', 'event', 'user', 'refreshUser')

  performLogin: (email, password, rememberMe) ->
    return (dispatch, getState) ->
      dispatch(actions.loginClicked(email))
      post(LOGIN_URL,
        email: email
        password: password
      ).then((json) ->
        dispatch(actions.receiveLogin(json))
        saveToken(json.token, rememberMe)
        if not json.error
          dispatch(API.retrieveUserLinks())
          dispatch(pushState null, '/')

          if json.user
            ga('set', 'userId', json.user.id)
            ga('send', 'event', 'user', 'login')
      )

  registerUser: (email, password, fullName, candidate=null) ->
    return (dispatch, getState) ->
      dispatch(actions.registerClicked(email))
      state = getState()
      post(REGISTER_URL,
        email: email
        password: password
        fullName: fullName
        candidate: candidate
        savedUrlIds: state.anonymousLinkIds
      ).then((json) ->
        dispatch(actions.registerReceived(json))
        saveToken(json.token)
        if not json.error
          dispatch(API.retrieveUserLinks())
          dispatch(pushState null, '/')

          if json.user
            ga('set', 'userId', json.user.id)
            ga('send', 'event', 'user', 'register')

      )

  changePassword: (newPassword) ->
    return (dispatch) ->
      post(CHANGE_PASSWORD_URL,
        password: newPassword
      ).then (json) ->
        dispatch(actions.passwordChanged(json))

  coinbaseAuthRedirect: ->
    return (dispatch) ->
      get(COINBASE_AUTH_REDIRECT_URL,
        redirectUri: "#{window.location.origin}/my_account"
      ).then (json) ->
        window.location = json.url

  coinbaseAuth: (code, state) ->
    return (dispatch) ->
      post(COINBASE_AUTH_URL,
        code: code,
        state: state
        redirectUri: "#{window.location.origin}/my_account"
      ).then (json) ->
        dispatch(API.refreshUser())
        dispatch(actions.coinbaseAuthReceived(json))
        track('coinbase', 'connect')

  getPendingPayouts: () ->
    return (dispatch) ->
      get(GET_PENDING_PAYOUTS_URL).then (json) ->
        dispatch(actions.pendingPayoutsReceived(json))

  confirmPayouts: (payouts) ->
    return (dispatch) ->
      post(CONFIRM_PAYOUTS_URL,
        payouts: payouts
      ).then (json) ->
        dispatch(actions.confirmPayoutsReceived(json))

  withdrawBalance: (amount) ->
    return (dispatch) ->
      post(WITHDRAW_BALANCE_URL,
        amount: amount
      ).then (json) ->
        dispatch(API.refreshUser())
        dispatch(actions.withdrawBalanceReceived(json))
        track('balance', 'withdraw')

  coinbaseDeauthorize: () ->
    return (dispatch) ->
      post(COINBASE_DEAUTH_URL).then (json) ->
        dispatch(API.refreshUser())
        dispatch(actions.coinbaseDeauthorizeReceived(json))
        track('coinbase', 'disconnect')

  saveName: (name) ->
    return (dispatch) ->
      post(UPDATE_NAME_URL,
        fullName: name
      ).then (json) ->
        dispatch(API.refreshUser())
        dispatch(actions.nameUpdated(json))
        track('user', 'setName')

  verifyUser: (token) ->
    return (dispatch) ->
      post(VERIFY_USER_URL+token).then (json) ->
        saveToken(json.token)
        dispatch(actions.userVerified(json))

  resetPassword: (email) ->
    return (dispatch) ->
      post(RESET_PASSWORD_URL,
        email: email
      ).then (json) ->
        dispatch(actions.passwordReset(json))

  setPassword: (password, token) ->
    return (dispatch) ->
      post(SET_PASSWORD_URL,
        password: password
        token: token
      ).then (json) ->
        dispatch(actions.passwordSet(json))

  setCandidate: (candidate) ->
    return (dispatch) ->
      post(SET_CANDIDATE_URL,
        candidate: candidate
      ).then (json) ->
        dispatch(API.refreshUser())
        dispatch(actions.candidateSet(json))
        ga('send', 'event', 'candidate', 'set')

}

track = (category, user) ->
  ga('send', 'event', category, user)

module.exports = API


