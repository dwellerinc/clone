
module.exports = {
  'kasich':
    name: 'John Kasich'
    image: '/images/candidates/johnkasich.png'
    party: 'R'

  'cruz':
    name: 'Ted Cruz'
    image: '/images/candidates/tedcruz.png'
    party: 'R'

  'trump':
    name: 'Donald Trump'
    image: '/images/candidates/donaldtrump.png'
    party: 'R'

  'rubio':
    name: 'Marco Rubio'
    image: '/images/candidates/marcorubio.png'
    party: 'R'

  'sanders':
    name: 'Bernie Sanders'
    image: '/images/candidates/berniesanders.png'
    party: 'D'

  'clinton':
    name: 'Hillary Clinton'
    image: '/images/candidates/hillaryclinton.png'
    party: 'D'

}
