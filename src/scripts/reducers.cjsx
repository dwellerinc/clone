{ combineReducers } = require 'redux'
{ routerStateReducer } = require 'redux-router'
_ = require 'underscore'


auth = (state={user: {}}, action) ->
  state = _.clone state

  switch action.type
    when '@@reduxReactRouter/routerDidChange'
      return _.extend state, {
        error: null,
        verifyError: false,
        passwordReset: false
        passwordSetSuccess: false
        emailVerified: false
      }
    when 'LOGIN_SUCCESS', 'REGISTER_SUCCESS'
      return _.extend state, {error: null, user: action.user, token: action.token}
    when 'REFRESH_USER_RECEIVED'
      if state.emailVerified
        return state
      return _.extend state, {user: action.user}
    when 'COINBASE_AUTH_SUCCESS'
      user = _.extend state.user, {coinbaseAuth: true}
      return _.extend state, {user: user}
    when 'REGISTER_FAIL', 'LOGIN_FAIL'
      return _.extend state, {error: action.error}
    when 'REGISTER_CLICKED', 'LOGIN_CLICKED'
      return _.extend state, {error: null}
    when 'LOGGED_OUT'
      return {}
    when 'EMAIL_VERIFY_SUCCESS'
      return _.extend state, {emailVerified: true, error: null, user: action.user, token: action.token}
    when 'EMAIL_VERIFY_FAIL'
      return _.extend state, {verifyError: true}
    when 'PASSWORD_RESET'
      return _.extend state, {passwordReset: true}
    when 'PASSWORD_SET'
      return _.extend state, {passwordSetSuccess: true}
    when 'PASSWORD_SET_FAILED'
      return _.extend state, {passwordSetSuccess: false, passwordSetError: action.error}
    when 'CANDIDATE_SET_SUCCESS'
      user = Object.assign({}, state.user)
      user?.candidate = action.candidate
      return _.extend state, {user: user}
    else
      return state

currentGeneratedUrl = (state={}, action) ->
  state = _.clone state
  switch action.type
    when 'GENERATE_LINK_SUCCESS'
      return _.extend state, {original:action.originalUrl, url: action.shortUrl}
    when 'GENERATE_LINK_FAIL'
      return _.extend state, {url: null, error: action.error}
    when '@@reduxReactRouter/routerDidChange'
      return _.extend state, {url: null, error:null}
    when 'LOGGED_OUT', 'CLEAR_GENERATED_LINK'
      return {}

    else
      return state

anonymousLinkIds = (state=[], action) ->
  switch(action.type)
    when 'GENERATE_LINK_SUCCESS_ANONYMOUS'
      state.push(action.id)
      return state
    else
      return state


links = (state=[], action) ->
  switch action.type
    when 'RECEIVE_LINKS'
      return action.links or []
    when 'LOGGED_OUT'
      return []
    else
      return state

pendingPayouts = (state=[], action) ->
  switch action.type
    when 'PENDING_PAYOUTS_RECEIVED'
      return action.users
    when 'CONFIRM_PAYOUTS_RECEIVED'
      return []
    when 'SET_PAYOUTS'
      action.payouts
    else
      return state

selectedLink = (state=null, action) ->
  switch action.type
    when 'LINK_SELECTED'
      return action.selectedLink
    when 'RECEIVE_LINKS'
      return action.links?[0] or null
    else
      return state

googleAnalytics = (state={}, action) ->
  switch action.type
    when '@@reduxReactRouter/routerDidChange'
      ga('send', 'pageview', action.payload.location.pathname)
      return state

    else
      return state

coinbase = (state={}, action) ->
  switch action.type
    when '@@reduxReactRouter/routerDidChange'
      return _.extend state, {message: null, success: null}
    when "WITHDRAW_BALANCE_RECEIVED"
      if action.success
        return _.extend {success: true, message: 'Your balance has been sent to your Coinbase account'}
      else
        return _.extend {
          success: false,
          message: 'There was a problem withdrawing your money. Please try again and contact us at support@truvi.co if the problem persists'
        }
    else
      return state


reducer = combineReducers({
  router: routerStateReducer
  auth
  currentGeneratedUrl
  links
  pendingPayouts
  selectedLink
  googleAnalytics
  coinbase
  anonymousLinkIds
})


module.exports = reducer
