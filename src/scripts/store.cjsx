{ createStore, applyMiddleware, compose } = require 'redux'
{ reduxReactRouter } = require 'redux-router'
thunkMiddleware = require 'redux-thunk'
createLogger = require 'redux-logger'
reducer = require './reducers'
{ createHistory } = require 'history'

if __DEVTOOLS__
  { devTools } = require 'redux-devtools'

routes = require './routes'

loggerMiddleware = createLogger()

createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware
  loggerMiddleware
)

if __DEVTOOLS__
  store = compose(
    createStoreWithMiddleware
    reduxReactRouter({
      routes
      createHistory
    })
    devTools()
  )(createStore)(reducer)
else
  store = compose(
    createStoreWithMiddleware
    reduxReactRouter({
      routes
      createHistory
    })
  )(createStore)(reducer)

module.exports = store

