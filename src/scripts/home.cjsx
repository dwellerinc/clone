{ connect } = require 'react-redux'
{ PropTypes } = require 'react'
{ generateLink, retrieveUserLinks } = require './actions/api'
GenerateLink = require './generate_link'
HowItWorks = require './how_it_works'
{ clearGeneratedLink } = require './actions/actions'

style =
  topDiv:
    marginBottom: 30
    paddingBottom:40
    paddingTop: 50

  bottomDiv:
    backgroundColor:'white'
    paddingLeft:'30%'
    paddingRight:'30%'
    marginLeft:'-50%'
    marginRight:'-50%'
    paddingTop: 80
    paddingBottom: 50
    flexGrow: 2


Home = React.createClass
  displayName: 'Home'
  mixins: []

  getInitialState: ->
    {}

  render: ->

    <div className=''>
      <div style={style.topDiv}>
        <GenerateLink generateLink={@props.generateLink}
          generatedLink={@props.generatedLink}
          linkError={@props.linkError}
          originalUrl={@props.originalUrl}
          user={@props.user}
          clearGeneratedLink={@props.clearGeneratedLink}
          retrieveLinks={@props.retrieveLinks}
          />
      </div>
    </div>


Home.propTypes = {
  generateLink: PropTypes.func.isRequired
  generatedLink: PropTypes.string
  linkError: PropTypes.string
  originalUrl: PropTypes.string
  user: PropTypes.object
  clearGeneratedLink: PropTypes.func.isRequired
}

mapStateToProps = (state) ->
  return {
    generatedLink: state.currentGeneratedUrl.url
    originalUrl: state.currentGeneratedUrl.original
    linkError: state.currentGeneratedUrl.error
    user: state.auth.user
  }

mapDispatchToProps = (dispatch) ->
  return {
    generateLink: (url, loggedIn) ->
      dispatch(generateLink(url, loggedIn))
    clearGeneratedLink: ->
      dispatch(clearGeneratedLink())
    retrieveLinks: ->
      dispatch(retrieveUserLinks())
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(Home)

