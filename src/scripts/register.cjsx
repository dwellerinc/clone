PropTypes = require('react').PropTypes
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ Link } = require 'react-router'
classnames = require 'classnames'
CandidateDropdown = require './politics/candidate_dropdown'
AlertButton = require './util/alert_button'

style =
  candidate:
    marginBottom: 15

  header:
    marginBottom: 30
    textAlign: 'center'

  submit:
    width: '100%'

  prompt:
    textAlign: 'center'
    marginBottom: '2rem'

  login:
    marginTop: 20
    textAlign: 'center'

  input:
    width:'100%'
    marginBottom:15
    border:'0px solid #fff'


module.exports = Register = React.createClass
  displayName: 'Register'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    fullName: ''
    email: ''
    password: ''
    confirmPassword: ''
    candidate: ''

  render: ->
    if @props.error
      errorMessage=<div className='ui warning message'>{@props.error}</div>

    pageclassnames=classnames({
      "ui", "two":$(window).width() > 768, "column", "grid", "centered",
    })

    <div className={pageclassnames}>
      <div className='column'>
        <div style={style.header} className='row'>
          <h1>Create a new account</h1>
        </div>
        <div style={style.prompt} className='row'>
          <h4>Get started by filling in your credentials below</h4>
        </div>
        <form className='row form-box'>
          <div className='ui big input' style={style.input}>
            <input placeholder='Full Name' type='text' valueLink={@linkState 'fullName'} />
          </div>

          <div className='ui big input' style={style.input}>
            <input placeholder='Email Address' type='text' valueLink={@linkState 'email'} />
          </div>

          <div className='ui big input' style={style.input}>
            <input placeholder='Password' type='password' valueLink={@linkState 'password'} />
          </div>

          <div style={style.candidate}>
            <CandidateDropdown onChange={@candidateChanged}/>
          </div>

          <div>
            <AlertButton buttonText="What happens when I pick a candidate?"
              alertTitle="Presidential Candidates"
              alertText="By choosing a candidate for the 2016 presidential election, your earnings
              are donated to the candidate's campaign."/>
          </div>

          <button className='ui button big-button' style={style.submit} onClick={@submit}>Register</button>

          {errorMessage}
        </form>

        <div style={style.login} className='row'>
          <Link to='signin'>Already have an account?</Link>
        </div>
      </div>
    </div>

  candidateChanged: (candidate) ->
    @setState {candidate: candidate}

  submit: (e) ->
    e.preventDefault()
    #if @state.password != @state.confirmPassword
    #  @props.registerFailed('Passwords do not match')
    #  return

    @props.registerUser(@state.email, @state.password, @state.fullName, @state.candidate)


Register.propTypes = {
  registerUser: PropTypes.func.isRequired
  error: PropTypes.string
  registerFailed: PropTypes.func.isRequired
}


