LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ connect } = require 'react-redux'
{ PropTypes } = require 'react'
classnames = require 'classnames'

URL_REGEX = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i

style =
  input:
    paddingLeft:50
    borderRadius: '50px !important'

  inputContainer:
    width: '100%'

  row:
    marginBottom:15

  form:
    marginTop: 35

  spacer:
    marginBottom:70

  subheader:
    maxWidth:'30rem'
    margin:'0 auto'
    lineHeight:'130%'


module.exports = GenerateLink = React.createClass
  displayName: 'GenerateLinkForm'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    originalUrl: ''
    isValid: false
    generatedLink: ''

  render: ->
    <div className=''>
      <div className='row' style={style.row}>
        <div className='hugeheader'>Support your candidate by sharing</div>
      </div>
      <div className='row' style={style.spacer}>
        <h4 style={style.subheader}>Truvi places an ad before your link destination and donates to your chosen candidate every time someone clicks your link</h4>
      </div>
      <div className='row' style={style.form}>
        <div style={style.inputContainer} className='ui big left icon input'>
          <i className='world icon'></i>
          <input type='text' placeholder='Paste a link to shorten'
            style={style.input}
            valueLink={@linkState 'originalUrl'}
            onKeyPress={@handleKeyPress}
            />
        </div>
      </div>
      <div className='row'>
        {@props.error}
      </div>
    </div>

  handleKeyPress: (e) ->
    if e.key == 'Enter'
      @submit()

  submit: (url) ->
    if not url
      url = @state.originalUrl

    loggedIn = @props.user?
    @props.generateLink(@state.originalUrl, loggedIn)

  onLinkChange: (event) ->
    url = event.target.value
    valid = URL_REGEX.test(url)
    if valid
      @submit(url)

    @setState {originalUrl: url, isValid: valid}


  testUrl: (url) ->
    URL_REGEX.test(url)

GenerateLink.propTypes = {
  generateLink: PropTypes.func.isRequired
  generatedLink: PropTypes.string
  error: PropTypes.string
  user: PropTypes.object
}

