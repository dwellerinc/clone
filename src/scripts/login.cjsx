PropTypes = require('react').PropTypes
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ Link } = require 'react-router'
classnames = require 'classnames'

style =
  login:
    marginBottom: '2rem'
    textAlign: 'center'
  submit:
    width: '100%'

  rememberMe:
    float: 'left'

  rememberMeLabel:
    color: 'white'
    fontSize: '0.7em'
    cursor: 'pointer'
    userSelect: 'none'
    WebkitTouchCallout: 'none'
    WebkitUserSelect: 'none'
    KhtmlUserSelect: 'none'
    MozUserSelect: 'none'
    msUserSelect: 'none'

  input:
    width:'100%'
    marginBottom:15
    border:'0px solid #fff'

  register:
    textAlign: 'center'
    marginTop: 20

  forgotPassword:
    float: 'right'

  optionsRow:
    height: 30

module.exports = Login = React.createClass
  displayName: 'Login'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    email: ''
    password: ''
    rememberMe: false

  render: ->
    if @props.error
      errorMessage=<div className='ui warning message'>{@props.error}</div>
    pageclassnames=classnames({
      "ui", "two":$(window).width() > 768, "column", "grid", "centered",
    })

    <div className={pageclassnames}>
      <div className='center aligned column'>

        <h1>Sign in</h1>
        <div style={style.login} className='row'>
          <h4>Fill in your credentials below</h4>
        </div>

        <div className='row form-box'>
          <form onSubmit={@submit}>
            <div className='ui big input' style={style.input}>
              <input placeholder='Email address' type='text' valueLink={@linkState 'email'}/>
            </div>
            <div className='ui big input' style={style.input}>
              <input placeholder='Password' type='password' valueLink={@linkState 'password'}/>
            </div>

            <div style={style.optionsRow} className=''>
              <div style={style.rememberMe} className='ui checkbox'>
                <input type='checkbox' checkedLink={@linkState('rememberMe')} />
                <label onClick={@rememberMeLabelClicked} style={style.rememberMeLabel}>Remember Me</label>
              </div>
              <div style={style.forgotPassword}>
                <Link to='forgot_password'>Forgot your password?</Link>
              </div>
            </div>
            <button className='ui button big-button' style={style.submit} type='submit'>Sign in</button>

            {errorMessage}
          </form>
        </div>

        <div style={style.register} className='row'>
          <Link to='register'>Need an account?</Link>
        </div>

      </div>
    </div>

  submit: (e) ->
    e.preventDefault()
    @props.performLogin(@state.email, @state.password, @state.rememberMe)

  rememberMeLabelClicked: ->
    @setState {rememberMe: !@state.rememberMe}

Login.propTypes = {
  performLogin: PropTypes.func.isRequired
  error: PropTypes.string
}



