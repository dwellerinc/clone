# Load css first thing. It gets injected in the <head> in a <style> element by
# the Webpack style-loader.
require '../../public/main.css'

React = require 'react'
ReactDom = require 'react-dom'
# Assign React to Window so the Chrome React Dev Tools will work.
window.React = React

{ Provider } = require 'react-redux'
{ ReduxRouter } = require 'redux-router'
store = require './store'

if false and __DEVTOOLS__
  { DevTools, DebugPanel, LogMonitor } = require 'redux-devtools/lib/react'

  tools =
    <DebugPanel top right bottom>
      <DevTools store={store} monitor={LogMonitor} />
    </DebugPanel>

ReactDom.render((
  <div className='' style={height:'100%'}>
    <Provider store={store}>
      <ReduxRouter routes={store}>
      </ReduxRouter>
    </Provider>
    {tools}
  </div>
), document.getElementById 'app')

