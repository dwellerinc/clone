{ PropTypes } = require 'react'

CandidateSelector = require './candidate_selector'

style =
  payouttitle:
    marginBottom:'0rem'

  payoutdescription:
    opacity:'0.5'
    color:'white'
    fontSize:'0.7rem'
    marginTop:'0.2rem'

module.exports = PayoutSelector = React.createClass
  displayName: 'PayoutSelector'

  getInitialState: ->
    newCandidate: @props.initialCandidate
    bitcoinPayout: !@props.initialCandidate

  componentDidMount: ->
    $('.ui.checkbox').checkbox(
      onChange: @onChange
    )

  componentWillReceiveProps: (newProps) ->
    @setState {bitcoinPayout: !newProps.initialCandidate}

  render: ->
    if not @state.bitcoinPayout
      candidateSelector = <div className='row'>
        <CandidateSelector candidate={@props.initialCandidate}
          setCandidate={@props.setCandidate}
          onChange={@candidateChanged}
          totalCandidatePayout={@props.totalCandidatePayout}
          onCancel={@cancelPressed} />
      </div>

    <div>
      <h3>Payouts</h3>
      <div className='ui form'>
        <div className='grouped fields'>
          <div className='ui radio checkbox'>
            <input checked={'checked' if @state.bitcoinPayout == true} onChange={@onChange} type='radio' name='payout' />
            <label>
              <h4 className='ui header'>100% bitcoin
                <div style={style.payoutdescription} className='sub header'>All proceeds go to you through coinbase</div></h4>
            </label>
          </div>

          <div className='ui radio checkbox'>
            <input checked={'checked' if @state.bitcoinPayout == false} onChange={@onChange} type='radio' name='payout' />
            <label>
            <h4 className='ui header'>
              100% candidate
              <div style={style.payoutdescription} className='sub header'>
              All proceeds go to chosen presidential candidate</div></h4>
            </label>
          </div>
        </div>
      </div>

      {candidateSelector}
    </div>

  onChange: (e) ->
    if !@state.bitcoinPayout
      @props.setCandidate(null)
    @setState {bitcoinPayout: !@state.bitcoinPayout}

  cancelPressed: ->
    if not @props.initialCandidate
      @setState {bitcoinPayout: true}
      $('.ui.checkbox').checkbox('toggle')



PayoutSelector.propTypes = {
  initialCandidate: PropTypes.string
  setCandidate: PropTypes.func.isRequired
}



