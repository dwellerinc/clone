LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ PropTypes } = require 'react'
CandidateDropdown = require './candidate_dropdown'
Candidates = require '../constants/candidates'

style = {
  dropdownContainer:
    color: 'black'
    width: '100%'
  dropdown:
    width: '100%'
  candidateBox:
    border: '1px solid rgba(255,255,255,0.4)'
    borderRadius:'3px'
    padding: '0.9rem'

  amountRaised:
    textAlign: 'center'

  image:
    width: 50
    height: 50
    borderRadius: '50%'
    backgroundColor: 'white'
    float: 'left'
    marginRight: 10

  changeButton:
    height:'2.2rem'
    paddingTop: 6
    marginTop: 10

  amountRaised:
    color: 'white'
    fontSize:'0.7rem'
    opacity:1
    marginTop:'0.2rem'

  candidatetext:
    marginTop:'0rem'
    marginBottom:'0.1rem!important'

  candidate:
    marginTop:'1rem'

  candidatebuttons:
    marginTop:'0.7rem'

  candidatecancel:
    width:'48%'

  candidatesave:
    width:'48%'
    float:'right'

}

module.exports = CandidateSelector = React.createClass
  displayName: 'CandidateSelector'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    editing: not @props.candidate

  componentDidMount: ->
    @props.onChange?(@state.candidate)

  componentWillReceiveProps: (newProps) ->
    candidate = newProps.candidate
    if candidate != @state.candidate
      @setState {candidate: newProps.candidate}
      #@props.onChange(newProps.candidate)

  setEditing: ->
    @setState {editing: true}

  cancel: ->
    @props.onCancel?()
    @setState {editing: false}

  save: ->
    @setState {editing: false}
    @props.setCandidate(@state.candidate)


  formatTotalPayout: ->
    return @props.totalCandidatePayout?.toFixed(3) or '0.00'

  staticCandidateDiv: ->
    candidate = @props.candidate
    name = Candidates[candidate].name
    party = Candidates[candidate].party
    image = Candidates[candidate].image

    <div>
      <img src={image} style={style.image}></img>
      <div>
        <h3 style={style.candidatetext} className='ui header'>
          {name} ({party})
          <div className='sub header'>
            <p style={style.amountRaised}>You have raised ${@formatTotalPayout()}</p>
          </div>
        </h3>
      </div>
      <button onClick={@setEditing} className='connectButton' style={style.changeButton}>Change candidate</button>
    </div>


  candidateSelector: ->
    <div>
      <CandidateDropdown onChange={@onChange} initialCandidate={@props.candidate} />
      <div style={style.candidatebuttons}>
      <button style={style.candidatecancel} className='ui button big-secondary-button' onClick={@cancel}>Cancel</button>
      <button style={style.candidatesave} className='ui button big-button' onClick={@save}>Save</button>
      </div>
    </div>


  render: ->
    if not @state.editing
      candidateDiv = @staticCandidateDiv()
    else
      candidateDiv = @candidateSelector()

    <div style={style.candidate}>
    <h3>Presidential Candidate</h3>
      <div style={style.candidateBox}>
        {candidateDiv}
      </div>

    </div>

  onChange: (value) ->
    @setState {candidate: value}


CandidateSelector.propTypes = {
  onChange: PropTypes.func.isRequired
  initialCandidate: PropTypes.string
  onCancel: PropTypes.func
  setCandidate: PropTypes.func.isRequired
  totalCandidatePayout: PropTypes.number
}


