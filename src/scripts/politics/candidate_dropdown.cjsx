LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ PropTypes } = require 'react'
Candidates = require '../constants/candidates'

style = {
  dropdownContainer:
    color: 'black'
    width: '100%'
    height: 47
  dropdown:
    width: '100%'
  text:
    lineHeight: '22px'
    fontSize: 18
    position: 'relative'

  dropdownChoice:
    padding: '5px 5px 5px 10px !important'
  dropdownImage:
    marginBottom: '5px !important'

}


module.exports = CandidateDropdown = React.createClass
  displayName: 'CandidateDropdown'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    candidate: @props.initialCandidate or null

  componentDidMount: ->
    @props.onChange?(@state.candidate)
    @initDropdown()

  initDropdown: ->
    $('.ui.dropdown').dropdown(
      action: 'activate'
      onChange: @onChange
    )
    $('.ui.dropdown').dropdown('set selected', @props.initialCandidate)

  componentDidUpdate: ->
    #@initDropdown()

  componentWillReceiveProps: (newProps) ->
    candidate = newProps.initialCandidate
    if not @state.candidate
      @setState {candidate: newProps.initialCandidate}
      $('.ui.dropdown').dropdown('set selected', newProps.initialCandidate)
      #@props.onChange(newProps.candidate)

  candidateChoice: (candidate) ->
    <div style={style.dropdownChoice} className='item' data-value={candidate}>
      <img style={style.dropdownImage} className="ui avatar image" src={Candidates[candidate].image}></img>
      {Candidates[candidate].name}
    </div>


  render: ->
    textStyle = Object.assign({}, style.text)
    if @state.candidate?
      textStyle.bottom = 5

    <div>
      <div className='ui selection dropdown' style={style.dropdownContainer}>
        <input type='hidden' name='candidate' />
        <i className='dropdown icon'></i>
        <div style={textStyle} className='default text'>Select Presidential Candidate</div>
        <div className='menu'>
          {@candidateChoice('sanders')}
          {@candidateChoice('trump')}
          {@candidateChoice('rubio')}
          {@candidateChoice('cruz')}
          {@candidateChoice('kasich')}
          {@candidateChoice('clinton')}
        </div>
      </div>
    </div>

  onChange: (value, candidate, $selectedItem) ->
    @props.onChange?(value)
    @setState {candidate: value}


CandidateDropdown.propTypes = {
  onChange: PropTypes.func.isRequired
  candidate: PropTypes.string
}


