{ Link } = require 'react-router'
{ PropTypes } = require 'react'

rightStyle = {
  fontSize: '0.7rem'
  textTransform:'uppercase'
  letterSpacing:'0.05rem'
  opacity:'1'
}

leftStyle = {
  fontSize: '1rem'
  paddingLeft:'0.5rem'
}

link = {
  marginRight: '1.5rem'
}

logostyle = {
  margin:0
}

logocontainer = {

}

module.exports = NavBar = React.createClass
  displayName: 'NavBar'
  render: ->

    loggedIn =
      display: if @props.user? then 'inherit' else 'none'

    loggedOut =
      display: if @props.user? then 'none' else 'inherit'


    if @props.links.length
      links = <Link style={Object.assign(loggedIn, link)} to="/links">My Links</Link>

    <div className='ui container'>
      <div className='ui large top secondary menu'>
        <div className='left menu' style={leftStyle}>
          <Link to='/'><div style={logocontainer}><img style={logostyle} src='images/logo.svg'></img></div></Link>
        </div>
        <div className='right menu' style={rightStyle}>
          {links}
          <Link style={Object.assign(loggedIn, link)} to='/my_account'>My Account</Link>
          <Link style={Object.assign(loggedOut, link)} to='/signin'>Sign In</Link>
          <Link style={Object.assign(loggedOut, link)} to='/register'>Register</Link>
          <Link style={link} to='/faq'>FAQ</Link>
          <Link to='#' style={Object.assign(loggedIn, link)} onClick={@logout}>Log Out</Link>
        </div>
      </div>
    </div>

  logout: ->
    @props.logout()


NavBar.PropTypes = {
  user: PropTypes.object
  logout: PropTypes.func.isRequired
  links: PropTypes.array.isRequired
}
