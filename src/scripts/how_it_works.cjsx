
style = {
  page:
    backgroundColor: 'inherit'
    paddingTop: 30
    color: 'black !important'

  column:
    textAlign: 'center'


  imageRow:
    JsDisplay: 'flex'
    display: '-webkit-flex'
    display: 'flex'
    paddingLeft:'6%'
    paddingRight:'6%'

  header:
    color: 'black !important'
    marginBottom: 60

  subHeader:
    color: 'black !important'
    marginBottom: 5

  detail:
    color: 'grey !important'

  verticalcenter:
    JsDisplay: 'flex'
    display: '-webkit-flex'
    display: 'flex'
    WebkitFlexDirection: 'column'
    flexDirection: 'column'
    justifyContent: 'center'

  landingicon:
    position: 'relative'
    top: '50%'
    left:'50%'
    transform: 'translateY(-50%)translateX(-50%)'
    WebkitTransform: 'translateY(-50%)translateX(-50%)'


}

module.exports = HowItWorks = React.createClass
  displayName: 'HowItWorks'

  render: ->
    <div style={style.page} className='ui stackable three column grid centered'>
      <h1 style={style.header}>How it works</h1>
      <div style={style.imageRow} className='row'>
        <div style={style.column} className='column'>
          <div className='ui small image'>

            <img src='images/paste_link_icon.svg' style={style.landingicon} alt='paste link' />

          </div>
          <h4 style={style.subHeader}>Shorten</h4>
          <p style={style.detail}>Paste a link to something you like on Truvi</p>
        </div>
        <div style={style.column} className='column'>
          <div className='ui small image'>
            <img src='images/share_link_icon.svg' style={style.landingicon} alt='share link' />
          </div>
          <h4 style={style.subHeader}>Share</h4>
          <p style={style.detail}>Truvi generates a shortened link which you share on social media</p>
        </div>
        <div style={style.column} className='column'>
          <div className='ui small image'>
            <img src='images/earn_bitcoin_icon.svg' style={style.landingicon} alt='earn bitcoin' />
          </div>
          <h4 style={style.subHeader}>Support</h4>
          <p style={style.detail}>An ad is displayed when someone clicks your link and we donate to your chosen candidate every time someone clicks on your link</p>
        </div>
      </div>

    </div>

