PropTypes = require('react').PropTypes
{ performLogin, registerUser } = require './actions/api'
{ registerFailed } = require './actions/actions'
Login = require './login'
Register = require './register'

{ connect }  = require 'react-redux'

style = {
  page:
    marginBottom: 30

}

LoginRegister = React.createClass
  displayName: 'Login'

  getDefaultProps: ->
    register: false

  getInitialState: ->
    register: @props.register

  render: ->

    if not @state.register
      child = <Login performLogin={@props.performLogin}
        error={@props.error} />
    else
      child = <Register registerFailed={@props.registerFailed} registerUser={@props.registerUser} error={@props.error} />

    <div style={style.page}>
      {child}
    </div>

  submit: ->
    @props.performLogin(@state.username, @state.password)


LoginRegister.propTypes = {
  performLogin: PropTypes.func.isRequired
  registerUser: PropTypes.func.isRequired
}

mapStateToProps = (state) ->
  return {
    error: state.auth.error
  }

mapDispatchToProps = (dispatch) ->
  return {
    performLogin: (username, password, rememberMe) ->
      dispatch(performLogin(username, password, rememberMe))
    registerUser: (username, password, fullName, candidate=null) ->
      dispatch(registerUser(username, password, fullName, candidate))
    registerFailed: (error) ->
      dispatch(registerFailed(error))
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(LoginRegister)



