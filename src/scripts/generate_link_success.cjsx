
{ PropTypes } = require 'react'
{ Link } = require 'react-router'

style=
  input:
    width:'100%'
    margin:'0 auto'
    textAlign:'center'
    border:'1 solid #fff'
    borderRadius:'.28571429rem !important'
    opacity:'1'

  copyButton:
    display: 'none'
    height: '100%'
    backgroundColor: '#3085bd !important'
    padding: '5px !important'
    height: 40
    width: 60
    border: '1px solid #3085bd'
    borderRadius: '0 30px 30px 0'
    textAlign: 'center'
    color: 'white'

  row:
    textAlign: 'center'
    marginBottom:'2rem !important'
    fontSize:'0.9rem'

  column:
    textAlign: 'center'

  shortenAnother:
    marginTop: 40
    fontSize: 20

  header:
    marginBottom:'1.5rem'

  redirects:
    textAlign:'center'
    textTransform:'uppercase'
    opacity:'0.7'
    letterSpacing:'1.2'
    fontSize:'0.7rem !important'
    marginBottom:'0.1em !important'

  submit:
    width:'100%'

  generatedLink:
    marginBottom:'0.75rem'

  originalurl:
    wordWrap: 'break-word'


module.exports = GenerateLinkSuccess = React.createClass
  displayName: 'GenerateLinkSuccess'

  componentDidMount: ->
    @props.retrieveLinks()

  render: ->
    if @props.user?
      message = 'Now share this link to start getting paid!'
      actionButton = <button className='ui button big-button' style={style.submit} onClick={@resetLink} type='submit'>
        Generate another link
      </button>
    else
      message = 'Here\'s a link for you to try.'
      actionButton = <Link className='ui button big-button' to='/register' style={style.submit}>
        Register to get paid
      </Link>

    <div style={style.column} className='column'>
      <div className='row'>
        <h1 style={style.header}>Nice work!</h1>
      </div>
      <div className='row'>
        <p style={style.row}>{message}</p>
      </div>
      <div className='row form-box'>
        <div style={style.generatedLink} className='row'>
          <div style={style.input} className='ui big white action input'>
            <input type='text' style={style.input}
              value={@props.generatedLink} disabled />
            <button style={style.copyButton} className='ui teal right labeled button'>
              Copy
            </button>
          </div>
        </div>
        {actionButton}

        <div style={marginTop: 30} className='row'>
        <h4 style={style.redirects}>Link Redirects to:</h4>
        <h4 target='_blank' style={style.originalurl} href={@props.originalUrl}>
          {@props.originalUrl}
        </h4>
        </div>

      </div>

    </div>

  resetLink: ->
    @props.clearGeneratedLink()

GenerateLinkSuccess.PropTypes =
  generatedLink: PropTypes.string.isRequired
  originalUrl: PropTypes.string.isRequired
  user: PropTypes.object
  clearGeneratedLink: PropTypes.func.isRequired
  retrieveLinks: PropTypes.func.isRequired


