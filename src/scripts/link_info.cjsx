{ PropTypes } = require 'react'
moment = require 'moment'
Radium = require 'radium'

style =
  date:
    fontSize: '0.6rem'
    display: 'inline-block'
    textTransform: 'uppercase'
    opacity:0.7
    letterSpacing: 1.2

  short:
    float: 'right'
    fontSize: '0.6rem'
    textTransform: 'uppercase'

  title:
    fontSize: '0.9rem'
    fontWeight: 500
    marginBottom: 5
    lineHeight:'1.5rem'

  original:
    fontSize: '0.7rem'
    overflowWrap: "break-word"
    wordWrap: "break-word"

    "-ms-word-break": "break-all"
    # This is the dangerous one in WebKit, as it breaks things wherever #
    wordBreak: "break-all"
    # Instead use this non-standard one: #
    wordBreak: "break-word"

    # Adds a hyphen where the word breaks, if supported (No Blink) #
    "-ms-hyphens": "auto"
    "-moz-hyphens": "auto"
    "-webkit-hyphens": "auto"
    hyphens: 'auto'


  topRow:
    marginBottom: 5

  container:
    padding: 8
    cursor: 'pointer'
    border: '2px solid rgba(255,255,255,.3)'
    marginBottom: 15
    padding: 20
    borderRadius: '3px'

    ':hover':
      border: '2px solid rgba(255,255,255,1)'

  selected:
    border: '2px solid white'
    padding:20


LinkInfo = React.createClass
  displayName: 'LinkInfo'

  render: ->
    containerStyle = style.container
    if @props.selected
      containerStyle = Object.assign({}, containerStyle, style.selected)

    <div onClick={@onClick} style={containerStyle} className=''>
      <div style={style.topRow} className=''>
        <div style={style.date}>
          {@formatDate(@props.link.date)}
        </div>
        <div style={style.short}>
          {@formatUrl(@props.link.shortUrl)}
        </div>
      </div>
      <div style={style.title} className=''>
        {@props.link.title}
      </div>
      <div style={style.original}>
        <a href={@props.link.originalUrl} target='_blank'>
          {@formatUrl(@props.link.originalUrl)}
        </a>
      </div>
    </div>

  formatDate: (date) ->
    moment(date).format('MMM D')

  formatUrl: (url) ->
    url.substr(url.indexOf('://')+3)

  onClick: ->
    @props.onClick(@props.link)


module.exports = Radium(LinkInfo)

LinkInfo.PropTypes = {
  link: PropTypes.object.isRequired
  onClick: PropTypes.func.isRequired
  selected: PropTypes.bool.isRequired
}
