
style = {
  page:
    paddingBottom: 50
  header:
    marginBottom:'0.5rem'

}

module.exports = Terms = React.createClass
  displayName: 'term'

  render: ->
    <div style={style.page}>
      <h1 style={style.header}>Terms & Conditions</h1>
       <div className='sub header'>Updated February 15th, 2016</div>

      <h4>1. Terms</h4> <br />
      <p>
        By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.
      </p>
      <h4>2. Use License</h4>
      <p>
        <ol style={listStyleType: 'lower-alpha'}>
          <li>
            Permission is granted to temporarily download one copy of the materials (information or software) on Truvi's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
            <ol style={listStyleType:'lower-roman'}>
              <li>
                modify or copy the materials;
              </li>
              <li>
                use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
              </li>
              <li>
                attempt to decompile or reverse engineer any software contained on Truvi's web site;
              </li>
              <li>
                remove any copyright or other proprietary notations from the materials; or
              </li>
              <li>
                v	transfer the materials to another person or "mirror" the materials on any other server.
              </li>
            </ol>
          </li>
          <li>
            This license shall automatically terminate if you violate any of these restrictions and may be terminated by Truvi at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
            </li>
          </ol>
        </p>

        <h4>3. Disclaimer</h4>
        <br />
          <p>
          The materials on Truvi's web site are provided "as is". Truvi makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Truvi does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.
          </p>

        <h4>4. Limitations</h4>
        <br />
        <p>
        In no event shall Truvi or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Truvi's Internet site, even if Truvi or a Truvi authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
        </p>

        <h4>5. Revisions and Errata</h4>
        <br />
        <p>
          The materials appearing on Truvi's web site could include technical, typographical, or photographic errors. Truvi does not warrant that any of the materials on its web site are accurate, complete, or current. Truvi may make changes to the materials contained on its web site at any time without notice. Truvi does not, however, make any commitment to update the materials.
        </p>

        <h4>6. Links</h4>
        <br />
        <p>
          Truvi has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Truvi of the site. Use of any such linked web site is at the user's own risk.
        </p>

        <h4>7. Site Terms of Use Modifications</h4>
        <br />
        <p>
          Truvi may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.
        </p>

        <h4>8. Governing Law</h4>
        <br />
        <p>
          Any claim relating to Truvi's web site shall be governed by the laws of the State of California without regard to its conflict of law provisions.
        </p>
        General Terms and Conditions applicable to Use of a Web Site.

        <p>
          We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.
        </p>


    </div>
