require('react-tap-event-plugin')()

Link = require('react-router').Link
NavBar = require './nav_bar'
Footer = require './footer'
{ PropTypes } = require 'react'
{ connect } = require 'react-redux'
{ logout } = require './actions/actions'
{ refreshUser, retrieveUserLinks } = require './actions/api'
HowItWorks = require './how_it_works'
{ StyleRoot } = require 'radium'

style = {

  container:
    height: '100%'
    paddingTop: 40
    display: 'flex'
    JsDisplay: 'flex'
    flexDirection: 'column'
    justifyContent: 'space-between'
    WebkitOverflowScrolling: 'touch'

  childContainer:
    display: 'flex'
    JsDisplay: 'flex'
    flexDirection: 'column'
    justifyContent: 'space-around'

  footer:
    width: '100%'
    paddingTop: 50
    backgroundColor: 'white'
}

module.exports = App = React.createClass
  displayName: 'App'

  componentDidMount: ->
    @props.refreshUser()
    @props.retrieveLinks()

  t: ->

  componentDidUpdate: ->

  showHowItWorks: ->
    @props.location.pathname == '/'

  render: ->
    children = this.props.children or 'Landing Page'

    <div className='' id='main' style={style.container}>
      <NavBar links={@props.links} user={@props.user} logout={@props.logout} />
      <div style={style.childContainer} className='ui container'>
        {children}
      </div>
      <div style={style.footer}>
        <Footer user={@props.user}
          showHowItWorks={@showHowItWorks()} />
      </div>
    </div>

App.propTypes = {
  user: PropTypes.object
  logout: PropTypes.func.isRequired
  refreshUser: PropTypes.func.isRequired
  links: PropTypes.array.isRequired
}

mapStateToProps = (state) ->
  return {
    user: state.auth.user
    links: state.links
  }

mapDispatchToProps = (dispatch) ->
  return {
    refreshUser: ->
      dispatch(refreshUser())
    logout: ->
      dispatch(logout())
    retrieveLinks: ->
      dispatch(retrieveUserLinks())
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(App)
