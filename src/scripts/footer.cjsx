{ PropTypes } = require 'react'
HowItWorks = require './how_it_works'
{ Link } = require 'react-router'
Radium = require 'radium'

rightStyle = {
}

leftStyle = {
  marginLeft:'1.5rem'
}

link = {
  marginRight: '1.5rem'
  color: 'black !important'
}

style =
  footer:
    width: '100%'
    fontSize: 14
    margin:0
    backgroundColor: 'white'
    color: 'black'
    JsDisplay: 'flex'
    display: '-webkit-flex'
    display: 'flex'
    WebkitFlexDirection: 'column'
    flexDirection: 'column'
  inner:
    margin: '0 auto'
    flexDirection: 'row'
    marginBottom:'2rem'
  howItWorks:
    marginBottom: 65

  lastRightMenu:
    margin: 0

Footer = React.createClass
  displayName: 'Footer'

  getInitialState: ->
    showHowItWorks: @props.showHowItWorks

  componentWillReceiveProps: (newProps) ->
    @setState {
      showHowItWorks: newProps.showHowItWorks
    }

  howItWorks: ->
    if @state.showHowItWorks
      return <div className='ui container' style={style.howItWorks} >
        <HowItWorks />
      </div>

  render: ->
    loggedIn =
      display: if @props.user? then 'inherit' else 'none'

    loggedOut =
      display: if @props.user? then 'none' else 'inherit'

    <footer style={style.footer} className='ui footer secondary menu'>
      {@howItWorks()}
      <div style={style.inner} className='ui container'>
        <div id='left-footer' className='left menu' style={leftStyle}>
          ©2016
        </div>
        <div id='right-footer' className='right menu' style={rightStyle}>
          <Link style={link} to="/about">About</Link>
          <Link style={link} to='/terms'>Terms & Conditions</Link>
          <Link style={Object.assign({}, link, style.lastRightMenu)} to='mailto:support@truvi.co' target='_top'>Email us</Link>
        </div>
      </div>
    </footer>

module.exports = Radium(Footer)

Footer.PropTypes = {
  user: PropTypes.object.isRequired
  showHowItWorks: PropTypes.bool.isRequired
}

