{ PropTypes } = require 'react'
LinkInfo = require './link_info'
{ connect } = require 'react-redux'
{ retrieveUserLinks } = require './actions/api'
{ linkSelected } = require './actions/actions'
LinkMetrics = require './link_metrics'
Radium = require 'radium'

style =
  title:
      paddingLeft:'1rem'
  mobiletitle:
      paddingLeft:'1rem'

  links:
      overflow: 'hidden'
      marginBottom:'1rem'
      paddingLeft:'1rem'

    '@media (min-width: 827px)':
      paddingLeft:0


Links = React.createClass
  displayName: 'Links'

  componentDidMount: ->
    @props.retrieveLinks()

  componentWillReceiveProps: ->

  metricsStyle: ->
    metricsStyle = {}
    if not @props.selectedLink
      metricsStyle.display = 'none'

    return metricsStyle

  render: ->
    <div className='ui mobile reversed stackable sixteen column grid'>
      <div className='computer only row'>
        <h1 style={style.title}>My Links</h1>
      </div>
      <div style={style.links} className='seven wide column'>
        <div style={overflow: 'auto'}>
          {
            @props.links.map (link) =>
              selected = false
              if link == @props.selectedLink
                selected = true

              <div key={link.shortUrl}>
                <LinkInfo selected={selected} onClick={@props.linkSelected} link=link />
              </div>
          }
        </div>
      </div>
      <div style={@metricsStyle()} className='ui sticky fixed top nine wide column' id='metricssticky'>
        <LinkMetrics link={@props.selectedLink} />
      </div>
      <div style={float:'left', display: 'inline-block', marginTop: 30} className='mobile only row'>
        <h4 style={style.title}>My Links</h4>
      </div>

    </div>


Links=Radium(Links)

Links.PropTypes = {
  links: PropTypes.array.isRequired
  linkSelected: PropTypes.func.isRequired
}

mapStateToProps = (state) ->
  links: state.links or []
  selectedLink: state.selectedLink

mapDispatchToProps = (dispatch) ->
  retrieveLinks: ->
    dispatch(retrieveUserLinks())
  linkSelected: (link) ->
    dispatch(linkSelected(link))

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(Links)

