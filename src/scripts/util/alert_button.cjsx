
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ PropTypes } = require 'react'

style = {
  button:
    marginBottom: 15
    width: '100%'
    height: 44
    border: '1px solid white'
    background: 0

  modal:
    textAlign: 'center'

  confirmButton:
    textAlign: 'center'

  header:
    textAlign: 'center'

  content:
    textAlign: 'center'

}


module.exports = AlertButton = React.createClass
  displayName: 'AlertButton'
  mixins: [LinkedStateMixin]

  componentDidMount: ->
    $('.ui.modal').modal()

  render: ->
    <div>
      <button type='button' style={style.button} onClick={@onClick}>
        {@props.buttonText}
      </button>

      <div style={style.modal} className="ui basic modal">
        <div style={style.header} className="header">
          {@props.alertTitle}
        </div>
        <div style={style.content} className="content">
          <p>{@props.alertText}</p>
        </div>
        <div style={style.confirmButton} className="actions">
          <div style={style.confirmButton} className="ui basic inverted approve button">
            Got it!
          </div>
        </div>
      </div>
    </div>


  onClick: (e) ->
    e.preventDefault()
    $('.ui.modal').modal('show')

