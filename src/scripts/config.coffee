
BACKEND_URL = process.env.BACKEND_URL || 'https://clone-backend.herokuapp.com/'

if __DEVELOPMENT__
  BACKEND_URL = 'http://localhost:8090/'

module.exports = {
  BACKEND_URL: BACKEND_URL
}
