{ Route, IndexRoute } = require 'react-router'
App = require './app'
LoginRegister = require './login_register'
GenerateLink = require './generate_link'
Links = require './links'
MyAccount = require './my_account'
Admin = require './admin/admin'
About = require './about'
Contact = require './contact'
Faq = require './faq'
Home = require './home'
Terms = require './terms'
AccountVerify = require './user/account_verify'
ForgotPassword = require './user/forgot_password'
ResetPassword = require './user/reset_password'

Register = React.createClass
  render: ->
    <LoginRegister register={true} />

Login = React.createClass
  render: ->
    <LoginRegister register={false} />

routes = (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path='signin' component={Login} />
    <Route path='register' component={Register} />
    <Route path='links' component={Links} />
    <Route path='my_account' component={MyAccount} />
    <Route path='admin' component={Admin} />
    <Route path='about' component={About} />
    <Route path='contact' component={Contact} />
    <Route path='faq' component={Faq} />
    <Route path='terms' component={Terms} />
    <Route path='account/verify/:token' component={AccountVerify} />
    <Route path='forgot_password' component={ForgotPassword} />
    <Route path='account/reset_password/:token' component={ResetPassword} />

  </Route>
)

module.exports = routes
