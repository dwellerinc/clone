{ PropTypes } = require 'react'
LinkedStateMixin = require 'react-addons-linked-state-mixin'
_ = require 'underscore'

module.exports = PendingPayout = React.createClass
  displayName: 'PendingPayout'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    payout = _.reduce @props.user.links, (sum, link) ->
      return sum + link.recommendedPayout
    , 0

    return {
      payout: payout
    }

  componentDidMount: ->
    @props.payoutChanged(@props.user, @state.payout)

  render: ->
    user = @props.user
    if not user.links
      return <div />
    <div>
      User: {user.email} <br />
      Candidate: {user.candidate}
      {
        user.links.map (link) =>
          console.log link
          metrics = link.metrics
          <div>
            Link: {link.originalUrl} <br />
            Clicks: {metrics.clicks} <br />
            Impressions: {metrics.impressions} <br />
            Destination Clicks: {metrics.destinationClicks} <br />
            Recommended Payout: ${link.recommendedPayout} <br />
          </div>
      }

      Actual Payout: $
      <input style={width: '100px'}
        type='text'
        onChange={@payoutChanged}
        value={@state.payout} />
    </div>

  payoutChanged: (event) ->
    payout = event.target.value
    @setState {payout: payout}

    @props.payoutChanged(@props.user, payout)

PendingPayout.propTypes = {
  user: PropTypes.object.isRequired
  payoutChanged: PropTypes.func.isRequired
}

