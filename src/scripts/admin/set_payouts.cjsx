{ PropTypes } = require 'react'
PendingPayout = require './pending_payout'
_ = require 'underscore'

module.exports = SetPayouts = React.createClass
  displayName: 'SetPayouts'

  getInitialState: ->
    pendingPayouts: @props.pendingPayouts

  render: ->
    <div>
      {
        @props.pendingPayouts.map (user) =>
          console.log user
          <div>
            <PendingPayout user={user} payoutChanged={@payoutChanged} />
            <br />
          </div>
      }
      <button onClick={@reviewPayouts}>Review</button>

    </div>

  reviewPayouts: ->
    @props.setPayouts(@state.pendingPayouts)
    @props.reviewPayouts()

  payoutChanged: (user, payout) ->
    pendingPayout = _.find @state.pendingPayouts, (payout) ->
      payout.email == user.email
    pendingPayout.payout = payout
    @setState({payouts:@state.pendingPayouts})

SetPayouts.propTypes = {
  pendingPayouts: PropTypes.array.isRequired
  setPayouts: PropTypes.func.isRequired
  reviewPayouts: PropTypes.func.isRequired
}


