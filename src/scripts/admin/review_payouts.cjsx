{ PropTypes } = require 'react'
_ = require 'underscore'

module.exports = ReviewPayouts = React.createClass
  displayName: 'ReviewPayouts'

  getInitialState: ->
    total = _.reduce @props.payouts, (total, payout) ->
      return total + payout.payout
    , 0

    total: total

  render: ->
    <div>
      {
        @props.payouts.map (payout) ->
          <div key={payout.email}>
            {payout.email}: ${payout.payout}
          </div>
      }

      <br />
      Total: ${@state.total}
      <br />
      <button onClick={@confirmPayouts}>Confirm</button>

    </div>

  confirmPayouts: ->
    confirmed = window.confirm("Confirm payout: $" + @state.total)
    if confirmed
      @props.confirmPayouts(@props.payouts)

ReviewPayouts.propTypes = {
  payouts: PropTypes.array.isRequired
  confirmPayouts: PropTypes.func.isRequired
}
