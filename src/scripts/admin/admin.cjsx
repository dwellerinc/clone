{ PropTypes } = require 'react'
{ connect } = require 'react-redux'
{ getPendingPayouts, confirmPayouts } = require '../actions/api'
{ setPayouts } = require '../actions/actions'
SetPayouts = require './set_payouts'
ReviewPayouts = require './review_payouts'

Admin = React.createClass
  displayName: 'Admin'

  componentDidMount: ->
    @props.getPendingPayouts()

  getInitialState: ->
    payouts: {}
    setPayouts: true

  render: ->
    if not @props.user?.admin
      return <div>
        Access Denied
      </div>

    if @state.setPayouts and @props.pendingPayouts.length
      childView = <SetPayouts
        setPayouts={@props.setPayouts}
        reviewPayouts={@reviewPayouts}
        pendingPayouts={@props.pendingPayouts} />
    else
      childView = <ReviewPayouts payouts={@props.pendingPayouts}
        confirmPayouts={@confirmPayouts} />

    <div>
      Admin <br /> <br />
      {childView}

    </div>

  reviewPayouts: ->
    @setState {setPayouts: false}

  confirmPayouts: (payouts) ->
    @props.confirmPayouts(payouts)
    @setState {setPayouts: true}

Admin.propTypes = {
  user: PropTypes.object.isRequired
  getPendingPayouts: PropTypes.func.isRequired
  setPayouts: PropTypes.func.isRequired
}

mapStateToProps = (state) ->
  return {
    user: state.auth.user
    pendingPayouts: state.pendingPayouts
  }

mapDispatchToProps = (dispatch) ->
  return {
    getPendingPayouts: () ->
      dispatch(getPendingPayouts())
    setPayouts: (payouts) ->
      dispatch(setPayouts(payouts))
    confirmPayouts: (payouts) ->
      dispatch(confirmPayouts(payouts))
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(Admin)

