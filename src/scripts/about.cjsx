style = {
  page:
    fontSize: '1rem'

}

module.exports = About = React.createClass
  displayName: 'About'

  render: ->
    <div style={style.page}>
      <p>
        Truvi is a URL shortener with a twist, we reward our sharers by offering a percentage of the advertising revenue. Our users have the ability to donate what they earn to their favorite candidate.
      </p>
      <p>
        Contact us at support@truvi.co with any questions or suggestions you might have.
      </p>
    </div>
