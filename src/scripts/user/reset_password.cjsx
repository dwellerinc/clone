{ PropTypes } = require 'react'
{ connect } = require 'react-redux'
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ setPassword } = require '../actions/api'
{ passwordSetFailed} = require '../actions/actions'
{ Link } = require 'react-router'

style =
  page:
    paddingBottom: 50
  login:
    marginBottom: '2rem'
    textAlign: 'center'
  submit:
    width: '100%'

  input:
    width:'100%'
    marginBottom:15
    border:'0px solid #fff'


ResetPassword = React.createClass
  displayName: 'ForgotPassword'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    password: ''
    confirmPassword: ''

  render: ->
    if @props.passwordSetError
      errorMessage=<div className='ui warning message'>{@props.passwordSetError}</div>

    if @props.passwordSetSuccess
      content = [
        <h1>Success</h1>
        <h4>Your new password was set successfully. Please sign in to contine. <Link to='/signin'>Sign In</Link></h4>
      ]

    else
      content = [
        <h1>Reset Password</h1>
        <div style={style.login} className='row'>
          <h4>Choose your new password</h4>
        </div>

        <div className='row form-box'>
          <form onSubmit={@submit}>

            <div className='ui big input' style={style.input}>
              <input placeholder='Password' type='password' valueLink={@linkState 'password'} />
            </div>

            <div className='ui big input' style={style.input}>
              <input placeholder='Confirm Password' type='password' valueLink={@linkState 'confirmPassword'} />
            </div>

            {errorMessage}

            <button className='ui button big-button' style={style.submit} type='submit'>Reset Password</button>
          </form>
        </div>
      ]


    <div style={style.page} className='ui two column grid centered'>
      <div className='center aligned column'>
        {content}
      </div>

    </div>

  submit: (e) ->
    e.preventDefault()
    if @state.password != @state.confirmPassword
      @props.passwordSetFailed('Passwords do not match')
      return

    @props.setPassword(@state.password, @props.params.token)


mapStateToProps = (state) ->
  return {
    passwordSetSuccess: state.auth.passwordSetSuccess
    passwordSetError: state.auth.passwordSetError
  }

mapDispatchToProps = (dispatch) ->
  return {
    setPassword: (password, token) ->
      dispatch(setPassword(password, token))
    passwordSetFailed: (error) ->
      dispatch(passwordSetFailed(error))
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(ResetPassword)

