{ PropTypes } = require 'react'
Radium = require 'radium'

style =
  button:
    width: '100%'
    background: 'none'
    borderRadius: 3
    fontWeight:400

ResetPasswordButton = React.createClass
  displayName: 'ResetPasswordButton'

  getInitialState: ->
    disabled: false

  componentDidMount: ->
    $('#forgot-password-button').popup({
      on: 'click'
    })


  componentWillReceiveProps: (newProps) ->
    if newProps.passwordReset
      i = 0
      #$('#forgot-password-button').popup('show', 'small')

  render: ->
    <div>
      <button className='connectButton' id="forgot-password-button" disabled={@state.disabled}
        data-title='' data-content="We've sent you an email with a link to set your new password"
        data-variation='tiny'
        data-position='right center'
        onClick={@onClick}
        style={style.button}>
          Reset Password
      </button>
    </div>

  onClick: ->
    @setState {disabled: true}
    @props.resetPassword(@props.user.email)

ResetPasswordButton=Radium(ResetPasswordButton)
module.exports=ResetPasswordButton

ResetPasswordButton.PropTypes = {
  passwordReset: PropTypes.bool.isRequired
  resetPassword: PropTypes.func.isRequired
  user: PropTypes.object.isRequired
}


