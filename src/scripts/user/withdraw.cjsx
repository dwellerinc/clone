{ PropTypes } = require 'react'
LinkedStateMixin = require 'react-addons-linked-state-mixin'
Radium = require 'radium'

inputStyle = {
  width: '100px'
}

style = {
  button:


    ':hover':
      border: '1px solid rgba(255,255,255,1)'

  page:
    paddingTop: 20

}

module.exports = Withdraw = React.createClass
  displayName: 'Withdraw'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    amount: @props.user.balance
    error: ''

  formatBalance: ->
    return @props.user.balance.toFixed(3)

  render: ->
    <div style={style.page} className='ui one column centered'>

      <p>Your Truvi balance is ${@formatBalance()}</p>
      <button className='connectButton' onClick={@withdraw} style={style.button}>Transfer bitcoin to Coinbase</button>
      {@state.error or @props.withdrawMessage}
    </div>


  withdraw: ->
    if @state.amount <= 0
      @setState {error: "No money available to withdraw"}
      return

    @setState {error: ''}
    @props.withdraw(@state.amount)


Withdraw=Radium(Withdraw)
module.exports=Withdraw

Withdraw.propTypes = {
  user: PropTypes.object.isRequired
  withdraw: PropTypes.func.isRequired
  withdrawMessage: PropTypes.string
}

