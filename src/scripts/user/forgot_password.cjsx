{ PropTypes } = require 'react'
{ connect } = require 'react-redux'
LinkedStateMixin = require 'react-addons-linked-state-mixin'
{ resetPassword } = require '../actions/api'

style =
  page:
    paddingBottom: 50
  login:
    marginBottom: '2rem'
    textAlign: 'center'
  submit:
    width: '100%'

  input:
    width:'100%'
    marginBottom:15
    border:'0px solid #fff'


ForgotPassword = React.createClass
  displayName: 'ForgotPassword'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    email: ''

  render: ->
    if @props.passwordReset
      content = [
        <h1>Success</h1>
        <h4>We've sent an email to your email address. Click the link in the email to reset your password</h4>
        <p>If you don't see the email, check other places it might be, like your junk, spam, social, or other folders.</p>
      ]

    else
      content = [
        <h1>Forgot Password?</h1>
        <div style={style.login} className='row'>
          <h4>Fill in your email below</h4>
        </div>

        <div className='row form-box'>
          <form onSubmit={@submit}>
            <div className='ui big input' style={style.input}>
              <input placeholder='Email address' type='text' valueLink={@linkState 'email'}/>
            </div>

            <button className='ui button big-button' style={style.submit} type='submit'>Reset Password</button>
          </form>
        </div>
      ]


    <div style={style.page} className='ui two column grid centered'>
      <div className='center aligned column'>
        {content}
      </div>

    </div>

  submit: (e) ->
    e.preventDefault()
    @props.resetPassword(@state.email)


mapStateToProps = (state) ->
  return {
    passwordReset: state.auth.passwordReset
  }

mapDispatchToProps = (dispatch) ->
  return {
    resetPassword: (email) ->
      dispatch(resetPassword(email))
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(ForgotPassword)
