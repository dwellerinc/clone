{ connect } = require 'react-redux'
{ PropTypes } = require 'react'
{ verifyUser } = require '../actions/api'
{ Link } = require 'react-router'
{ pushState } = require 'redux-router'

style =
  page:
    marginBottom: 50
  column:
    textAlign: 'center'
  button:
    width: '100%'


AccountVerify = React.createClass
  displayName: 'AccountVerify'

  componentDidMount: ->
    @props.verifyUser(@props.params.token)

  render: ->
    if true or @props.user
      <div style={style.page} className='ui three column centered grid'>
        <div style={style.column} className='column'>
          <h1>Nice Work!</h1>
          <h4>Your email has been verified</h4>
          <Link to='/' style={style.button} className='ui button big-button'>Generate Link</Link>
        </div>
      </div>
    else if @props.verifyError
      <div>Invalid URL</div>

  generateLinkClicked: (event) ->
    pushState null, '/'


mapStateToProps = (state) ->
  return {
    verifyError: state.auth.verifyError
    user: state.auth.user
  }

mapDispatchToProps = (dispatch) ->
  return {
    verifyUser: (token) ->
      dispatch(verifyUser(token))
  }

module.exports = connect(
  mapStateToProps
  mapDispatchToProps
)(AccountVerify)

