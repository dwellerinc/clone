{ PropTypes } = require 'react'
LinkedStateMixin = require 'react-addons-linked-state-mixin'

style = {
  label:
    color:'white'
    fontSize:'0.8rem'
    fontWeight:'500'
    display: 'block'

  staticName:
    display: 'inline-block'

  editButton:
    width: 100
    height: 35
    fontSize: '16px'
    float: 'right'

}

module.exports = NameEditor = React.createClass
  displayName: 'NameEditor'
  mixins: [LinkedStateMixin]

  getInitialState: ->
    editing: false
    name: @props.initialName

  componentWillReceiveProps: (newProps) ->
    if not @state.editing
      @setState {name: newProps.initialName}

  input: ->
    <div classNamfive='ui small input'>
      <input type='text'  valueLink={@linkState('name')} name='firstName' placeholder='John Doe'/>
      <button className='connectButton'  onClick={@cancel} style={style.saveButton} type='button'>Cancel</button>
      <button className='connectButton'  onClick={@saveName} style={style.saveButton} type='submit'>Save</button>
    </div>

  nameDisplay: ->
    <div>
      <label style={style.label}>Full Name</label>
      <p style={style.staticName}>{@props.initialName}</p>
      <button className='connectButton' style={style.editButton} type='button' onClick={@edit} >Edit</button>
    </div>


  render: ->
    if @state.editing
      input = @input()
    else
      input = @nameDisplay()

    <div>
      {input}
    </div>

  saveName: (e) ->
    e.preventDefault()
    @props.saveName(@state.name)
    @setState {editing: false}

  edit: (e) ->
    e.preventDefault()
    @setState {editing: true}

  cancel: ->
    @setState {editing: false,name:@props.initialName}




NameEditor.propTypes = {
  initialName: PropTypes.string
  saveName: PropTypes.func.isRequired
}


