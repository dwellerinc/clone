{ PropTypes } = require 'react'
moment = require 'moment'
Radium = require 'radium'
LinkedStateMixin = require 'react-addons-linked-state-mixin'

style = {
  page:
    margin: 0

  connectButton:
    background: 'none'
    border: '1px solid rgba(255, 255, 255, 0.55)'
    borderRadius: 30
    color: 'white'
    height: '2.5em'
    width:'100%'
    fontSize:'0.8rem'

    ':hover':
      border: '1px solid rgba(255,255,255,1)'

  row:
    paddingTop:0

  noleftpadding:
    paddingLeft:0

  smallbottommargin:
    marginBottom:'0.15rem !important'

  lowopacity:
    opacity:0.7

}

CoinbaseAuth = React.createClass
  displayName: 'CoinbaseAuth'

  getInitialState: ->
    {}

  componentDidMount: ->


  render: ->
    if not @props.user?.coinbase
      auth = [
        <div key='1' className='row'>
          Connect to a Coinbase account to withdraw money
        </div>
        <div key='2' style={style.row} className='row'>
          <button style={style.connectButton} onClick={@props.coinbaseAuthRedirect}>Connect Coinbase Account</button>
        </div>
      ]
    else
      auth = [
        <div key='3' className='column' style={style.noleftpadding}>
          <div className='row'><h4 style={style.smallbottommargin}>
            {@props.user?.coinbase?.name}</h4><p style={style.lowopacity}>Connected {@formatDate(@props.user?.coinbase?.dateConnected)}</p>
          </div>
          <div className='row'>

          </div>
        </div>
        <div key='4' className='column'>
          <button className='connectButton' onClick={@props.deauthorize}>Disconnect</button>
        </div>
      ]

    <div style={style.page} className='ui two column grid'>
      {auth}
    </div>

  formatDate: (date) ->
    moment(date).format('MM/DD/YY')

CoinbaseAuth=Radium(CoinbaseAuth)
module.exports=CoinbaseAuth

CoinbaseAuth.propTypes = {
  user: PropTypes.object.isRequired
  coinbaseAuthRedirect: PropTypes.func.isRequired
  deauthorize: PropTypes.func.isRequired
}



