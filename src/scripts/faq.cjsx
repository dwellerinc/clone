
style =
  title:
    color: 'white !important'
  question:
    borderTop: '1px solid rgba(255,255,255,0.1)'
    paddingTop: 10
    paddingBottom: 10
    fontWeight: 500
    fontSize:'1.3em'
  header:
    marginBottom: 30
  column:
    maxWidth: 600
  arrow:
    float: 'right'
  answer:
    marginBottom: 5
    fontSize:'0.8em'
  page:
    paddingBottom: 80

module.exports = Faq = React.createClass
  displayName: 'Faq'

  componentDidMount: ->
    $('.ui.accordion').accordion()

  questionsAnswers:
    'What is Truvi? What does it do?': 'Truvi is a URL shortener with a twist, we reward our sharers by offering a percentage of the advertising revenue. Our users have the ability to donate what they earn to their favorite candidate.'
    'What if I don\'t want to support any of these candidate?': 'You can keep the money to yourself :)'
    'What about after the elections?': 'We are planning on adding your favorite charities and organizations to allow you to support a cause of your choice.'
    'How much do I earn?': 'We pay $1 per 900 unique clicks to your links'
    'How will you pay me?': 'You will be paid in bitcoin to your coinbase wallet.'
    'How will you pay my candidate?': 'We will pay your candidate on a weekly basis.'
    'Do the political contributions count towards my 2700$ limit?': 'No, the money is donated on behalf of Truvi.'
    'Will my shortened links ever expire?': 'Your links will never expire'
    'When are payouts issued?': 'Payouts are issued every day and you can withdraw your bitcoin whenever you want.'
    'Can I shrink websites that contain adult material?': 'You may not shrink website URLs that contain or link to adult material. You will be banned from our website if you do.'
    'How do I delete / remove my account?': 'Please contact us at support@truvi.co'
    'Are you supporting other wallets?': 'We are using Coinbase for now, but we hope to support other wallets in the near future.'

  render: ->
    faqs = []
    for question,answer of @questionsAnswers
      faqs.push(
        <div key={question} style={style.question}>
          <div style={style.title} className='title'>
            {question}
            <i style={style.arrow} className='dropdown icon'></i>
          </div>
          <div style={style.answer} className='content'>
            <p className='transition hidden'>{answer}</p>
          </div>
        </div>
      )

    <div style={style.page} className='ui grid one column centered'>
      <div style={style.column} className='column'>
        <h1 style={style.header}>Frequently Asked Questions</h1>
        <div className='ui accordion'>
          {faqs}
        </div>
      </div>
    </div>

