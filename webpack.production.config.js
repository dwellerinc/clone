var path = require('path');
var webpack = require('webpack');
node_modules_dir = path.resolve(__dirname, 'node_modules');

module.exports = {
  entry: {
    app: [
      './src/scripts/router'
    ],
    vendors: ['react']
  },
//  devtool: 'source-map',
  output: {
      path: path.join(__dirname, "public"),
      filename: "bundle.js",
  },
  plugins: [
    new webpack.DefinePlugin({
      // This has effect on the react lib size.
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      },
      __DEVELOPMENT__: false,
      __DEVTOOLS__:false
    }),
    new webpack.EnvironmentPlugin([
      "NODE_ENV",
      "BACKEND_URL"
    ]),
    new webpack.IgnorePlugin(/vertx/),
    new webpack.IgnorePlugin(/un~$/),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
  ],
  resolve: {
    extensions: ['', '.js', '.cjsx', '.coffee']
  },
  module: {
    loaders: [
      { test: /\.css$/, loaders: ['style', 'css']},
      { test: /\.js$/, exclude:[node_modules_dir], loader:'babel'},
      { test: /\.cjsx$/, exclude:[node_modules_dir], loaders: ['coffee', 'cjsx']},
      { test: /\.coffee$/, exclude:[node_modules_dir], loader: 'coffee' }
    ]
  }
};
